<!DOCTYPE html>
<!--
CAB230 Web Computing Assignment 1
Author: Heath Mayocchi n9378201 
-->
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Search results - WiFi Hotspot Review</title>
		<script src="js/ass1script.js"></script>
		<!-- using defer src so the page can load up without waiting for the map script -->
		<script defer src="http://maps.googleapis.com/maps/api/js"></script>
		<link href="css/ass1style.css" rel="stylesheet" type="text/css" />
		<!-- assign input to a variable -->
		<?php include 'php/userSearchterm.inc'; ?>
	</head>
	<body onload="showResMap()">
	<?php include 'php/resMap.inc'; ?>
		<!-- Set a wrapper for page width and alignment -->
		<div id="wrapper">			
			<?php include 'php/header.inc'; ?>
			<?php include 'php/sidebar.inc'; ?>
			<div id="section">
				<div id="resultMap" class="rBorder reMap"></div>
				<h3>Search results for: <i><?php echo "{$searchTerm}"?></i></h3>
				<table id="results">
					<tr>
						<th>WiFi Hotspot</th>
						<th>Address</th>
						<th>Suburb</th>
					</tr>					
					<?php include 'php/results.inc'; ?>
				</table>
			</div><!-- end section -->
			<?php include 'php/footer.inc'; ?>
		</div><!-- end wrapper -->
		<div id="overlay"></div>
	</body>
</html>
