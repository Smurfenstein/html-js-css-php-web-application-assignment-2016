<?php
require_once 'php/pdoconnect.inc';
?>
<script>
/* Map toggle for closest search */
function mapOpen() {
	document.getElementById("overlay").style.display = "block";
}
function mapClose() {
	document.getElementById("overlay").style.display = "none";
}
/* Find user location using geolocation */
function UserLocation(){
	// Get user location
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showMap, showMapError);
	} else {
		document.getElementById("locationSupport").innerHTML="Geolocation is not supported by this browser.";
	}
}
/* Map for geolocation button */
function showMap(position) {
	/* Location coordinates */
	var userLatlon = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
<?php
	$sql = "SELECT lat, lng FROM items";	
	$sqlQuery = $conn->query($sql);
	$markerNum = 0;	
	foreach ($sqlQuery as $row) {
		// Combining a name and number for unique id's
		$thisMarker = "marker".$markerNum;
		$markerNum = $markerNum + 1;
		$lat = $row['lat'];
		$lng = $row['lng'];
		echo "var $thisMarker = new google.maps.LatLng($lat,$lng);";
	}
?>
	/* Display the map box and overlay*/
	document.getElementById("mapBox").style.display = "block";
	document.getElementById("overlay").style.display = "block";
	/* Set options for zoom, pan and map type */
	var mapProp = {
		center: userLatlon,
		zoom:14,
		panControl:true,
		zoomControl:true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	/* Display map with given options */
	var map = new google.maps.Map(document.getElementById("mapBox"),mapProp);
	/*
	* This is to create the close button
	* Create the DIV to hold the control and call the CenterControl()
		constructor passing in this DIV.
	*/
	var centerControlDiv = document.createElement('div');
	var centerControl = new CenterControl(centerControlDiv, map);
	centerControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);
<?php
	$sql = "SELECT name FROM items";	
	$sqlQuery = $conn->query($sql);
	$markerNum = 0;	
	foreach ($sqlQuery as $row) {
		$name = $row['name'];
		// Combining a name and number for unique id's
		$thisMarker = "marker".$markerNum;
		$markerNum = $markerNum + 1;
		// Output javascript to create the map markers
		echo "var marker".$thisMarker." = new google.maps.Marker({ 
		position: $thisMarker,";
		echo "title: \"$name\",";
		echo "});";
		echo "marker".$thisMarker.".setMap(map);";
		echo "google.maps.event.addListener(marker".$thisMarker.", 'click', function() {
		window.location.href = \"review.php?h=$name\";
		});";
	}
?>
}
google.maps.event.addDomListener(window, 'load', showMap);
</script>