<?php
// Add slashes to the name string to avoid sql query syntax errors
$userSearch = addslashes($userSearch);
// Statement for search by 'suburb'
$sqlSuburb = "SELECT name, address, suburb FROM items where suburb LIKE '$userSearch%'";
// Statement for search by 'name'
$sqlName = "SELECT name, address, suburb FROM items where name LIKE '$userSearch%'";
// Statement for search by 'rating'
$sqlRating = "SELECT DISTINCT items.name, items.address, items.suburb FROM items, reviews WHERE reviews.name = items.name and reviews.rating = '$userSearch%';";
$sqlArray = array("$sqlSuburb", "$sqlName", "$sqlRating");
	
$toReturn = 0;
foreach ($sqlArray as $query){
	$sqlQuery = $conn->query($query);
	if ($sqlQuery) {
		foreach ($sqlQuery as $row) {
			$name = $row['name'];
			echo "<tr>";
			echo "<td><a href=\"review.php?h=$name\">{$row['name']}</a></td>";
			echo "<td>{$row['address']}</td>";
			echo "<td>{$row['suburb']}</td>";
			echo "</tr>";
			$toReturn = 1;
		}
	}
	if ($toReturn == 1) {
		$toReturn = 0;
		return;
	}
}
echo "<div class=\"validateError\">";
echo "'{$searchTerm}' is not in our database, please search again";
echo "</div>";
?>