<?php
require_once 'php/pdoconnect.inc';

// Statement for hotspot 'name'
$sqlReview = "SELECT name, address, suburb FROM items where name = '$reviewHotspot'";
$sqlQuery = $conn->query($sqlReview);
if ($sqlQuery) {
	foreach ($sqlQuery as $row) {
		$name = $row['name'];
		$address = $row['address'];
		$suburb = $row['suburb'];
		echo "<h3>$name</h3>";
		echo "<h4>$address</h4>";
		echo "<h4>$suburb</h4>";
	}
}
	echo "</div>
	</div>
</div>
<div id=\"reviewSection\">
	<br>
	<h3>Reviews:</h3>";
// Statement for Hotspot details
$sqlContent = "SELECT members.fname, members.lname, reviews.reviewdate, reviews.rating, reviews.comment FROM members, reviews WHERE reviews.email = members.email AND reviews.name = '$reviewHotspot'";
$sqlConQuery = $conn->query($sqlContent);
if ($sqlConQuery) {
	$toReturn = 0;
	foreach ($sqlConQuery as $conRow) {
		$fname = $conRow['fname'];
		$lname = $conRow['lname'];
		$reviewdate = $conRow['reviewdate'];
		$rating = $conRow['rating'];
		$comment = $conRow['comment'];
		echo "<h4>$fname $lname</h4>";
		echo "<p>$reviewdate</p>";
		if ($rating == 1) {
			include 'php/starsOne.inc';
		}
		if ($rating == 2) {
			include 'php/starsTwo.inc';
		}
		if ($rating == 3) {
			include 'php/starsThree.inc';
		}
		if ($rating == 4) {
			include 'php/starsFour.inc';
		}
		if ($rating == 5) {
			include 'php/starsFive.inc';
		}
		echo "<p>$comment</p><hr><hr>";
		$toReturn = 1;
	}
	if ($toReturn == 1) {
		$toReturn = 0;
		return;
	}
}
// First review message, only display's if there are no records found in the DB
echo "<h4 class=\"validateError\">Be the first to review this Hotspot</h4><button id=\"newReview\"onclick=\"location.href='writeReview.php';\" class=\"button rBorder\">Write a review</button>";
?>



