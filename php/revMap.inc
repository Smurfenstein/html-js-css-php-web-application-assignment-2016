<?php
require_once 'php/pdoconnect.inc';
?>
<script>
/* Map for location review page */
function showRevMap() {
<?php	
	// Add slashes to the name string to avoid sql query syntax errors
	$reviewHotspot = addslashes($reviewHotspot);
	// Statement for search from 'name'
	$sqlReview = "SELECT name, lat, lng FROM items where name = '$reviewHotspot'";
	$sqlQuery = $conn->query($sqlReview);
	foreach ($sqlQuery as $row) {
		$name = $row['name'];		
		$lat = $row['lat'];
		$lng = $row['lng'];
		echo "var revLoc = new google.maps.LatLng($lat,$lng);";
		echo "/* Set options for zoom, pan and map type */
		var revProp = {
			center: revLoc,
			zoom: 16,
			panControl: true,
			zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};";
		echo "/* Display map with given options */
		var revMap = new google.maps.Map(document.getElementById(\"reviewMap\"),revProp);";
		echo "/* Map marker location */
		var revMarker = new google.maps.Marker({
			position: revLoc,
			title: \"$name\",
		});";
	}
	echo "/* setMap() used to display marker */
	revMarker.setMap(revMap);";
	echo "/* Add event listener for link to item */
	google.maps.event.addListener(revMarker, 'click', function() {
	window.location.href = \"review.php?h=$name\";
	});";
?>
	
	
}
google.maps.event.addDomListener(window, 'load', showRevMap);
</script>