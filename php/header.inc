<div id="header">
	<?php include 'php/pdoconnect.inc'; ?>
	<!-- Create logo with link back to index-->
	<div id="logo">
		<a href="index.php"><img src="images/wifi_logo.jpg" alt="WiFi Review Logo"/></a>
	</div>
	<?php include 'php/closestwifi.inc'; ?>
	<!-- Create closest WiFi search button -->
	<button id="closestWiFi" onclick="UserLocation()" class="button rBorder">
	Find your<br>Closest Hotspot
	</button>
	<!-- Form for the main search bar -->
	<form id="mainSearch" action="results.php" method="get">
		<input id="searchText" class="searchTxt" type="text" name="q" placeholder="Search by name or location" onkeyup="HideError('searchError')"><input id="searchSubmit" type="submit" class="button rBorder"  value="&gt;" onclick="return ValidateSearch()">
		<div id="searchError" class="validateError">
			Please use only letters and symbols, no numbers.
		</div>
	</form>
	<div id="welcome">Welcome: Guest</div>
	<div id="userlinks">
		<a href="signup.php">Sign Up</a><br>
		<a href="javascript:LoginBox();">Login</a>				
	</div>	
	<!-- popup login box, hidden by default -->
	<form action="php/userLogin.php" method="post">
		<div id="loginPopup" class="rBorder">
			<b>Login</b><a href="javascript:LoginClose();">x</a><br>
			<input id="loginEmail" type="text" name="loginID" class="loginText" placeholder="User email" onchange="HideError('loginEmailError')">
				<div id="loginEmailError" class="validateError">
					me@email.com
				</div>
			<input id="loginPassword" type="password" name="loginPass" class="loginText" placeholder="Password" onchange="HideError('loginPassError')">
				<div id="loginPassError" class="validateError">
					8 characters min
				</div>
			<input id="loginSubmit" type="submit" class="button rBorder" value="Submit" onclick="return ValidateLogin(this)">
		</div>
	</form>
	<!-- popup map box, hidden by default -->
	<div id="mapBox" class="mapClass rBorder"></div>			
</div><!-- end header -->