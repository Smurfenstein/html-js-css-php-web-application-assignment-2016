<?php
require_once 'php/pdoconnect.inc';
?>
<script>
function showResMap(){
<?php
	// Statement for search from 'suburb'
	$sqlSuburb = "SELECT lat, lng FROM items where suburb LIKE '$userSearch%'";
	// Statement for search from 'name'
	$sqlName = "SELECT lat, lng FROM items where name LIKE '$userSearch%'";
	// Statement for search from 'rating'
	$sqlRating = "SELECT items.lat, items.lng FROM items, reviews where reviews.name = items.name and reviews.rating = '$userSearch%'";
	$sqlArray = array("$sqlSuburb", "$sqlName", "$sqlRating");
	$toBreak = 0;
	foreach ($sqlArray as $query){		
		$sqlQuery = $conn->query($query);
		$markerNum = 0;
		// Set map for each result
		foreach ($sqlQuery as $row) {
			// Combining a name and number for unique id's
			$thisMarker = "marker".$markerNum;
			$markerNum = $markerNum + 1;
			$lat = $row['lat'];
			$lng = $row['lng'];
			// Output javascript to create the map
			echo "var $thisMarker = new google.maps.LatLng($lat,$lng);";
			$toBreak = 1;
		}
		if ($toBreak == 1) {
			$toBreak = 0;
			break;
		}
	}
	$markerNum = 0;		
	echo "\n";
	echo "var searchProp = {";
	echo "center: $thisMarker,";
?>
		zoom: 14,
		panControl: true,
		zoomControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	/* Display map with given options */
	var searchMap = new google.maps.Map(document.getElementById("resultMap"),searchProp);
	/* Map marker location */
<?php
	// Statement for search from 'suburb'
	$sqlSuburb = "SELECT name FROM items where suburb LIKE '$userSearch%'";
	// Statement for search from 'name'
	$sqlName = "SELECT name FROM items where name LIKE '$userSearch%'";
	// Statement for search from 'rating'
	$sqlRating = "SELECT items.name FROM items, reviews where reviews.name = items.name and reviews.rating = '$userSearch%'";
	$sqlArray = array("$sqlSuburb", "$sqlName", "$sqlRating");
	$toBreak = 0;
	foreach ($sqlArray as $query){		
		$sqlQuery = $conn->query($query);
		$markerNum = 0;	
		// Set map for each result
		foreach ($sqlQuery as $row) {
			$name = $row['name'];
			// Combining a name and number for unique id's
			$thisMarker = "marker".$markerNum;
			$markerNum = $markerNum + 1;
			// Output javascript to create the map markers
			echo "var marker".$thisMarker." = new google.maps.Marker({ 
			position: $thisMarker,";
			echo "title: \"$name\",";
			echo "});";
			echo "marker".$thisMarker.".setMap(searchMap);";
			echo "google.maps.event.addListener(marker".$thisMarker.", 'click', function() {
			window.location.href = \"review.php?h=$name\";
			});";
			$toBreak = 1;
		}
		if ($toBreak == 1) {
			$toBreak = 0;
			break;
		}
	}	
?>
}
google.maps.event.addDomListener(window, 'load', showResMap);
</script>