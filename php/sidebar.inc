<div id="sidebar">
	<h3>Quick Search</h3>
	<!-- Set dropdown class for menu positioning -->
	<div class="dropdown">
		<!-- Create menu button with toggle function -->
		<button onclick="SuburbDropFunction()" class="suburbdropbtn button rBorder">
		Suburbs	▾
		</button>
		<div id="suburbDropdown" class="suburbContent dropdownContent rBorder">
			<?php include 'suburbDropdown.inc'; ?>
		</div><!-- end suburbDropdown -->
		<br>
		<!-- Create menu button with toggle function -->
		<button onclick="RatingDropFunction()" class="ratingdropbtn button rBorder">
		Rating	▾
		</button>
		<div id="ratingDropdown" class="ratingContent dropdownContent rBorder">
			<a href="results.php?q=One Star">One Star</a>
			<a href="results.php?q=Two Stars">Two Stars</a>
			<a href="results.php?q=Three Stars">Three Stars</a>
			<a href="results.php?q=Four Stars">Four Stars</a>
			<a href="results.php?q=Five Stars">Five Stars</a>
		</div><!-- end ratingDropdown -->
	</div><!-- end dropdown class -->
	<!-- Create button for writing a review -->
	<br><h3>Have your say</h3>
	<div id="writeReviewBtn">
	<button onclick="location.href='writeReview.php';" class="button rBorder">Write a<br>review</button>
	</div>
</div><!-- end sidebar -->
