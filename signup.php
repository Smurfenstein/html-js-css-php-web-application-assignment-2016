<!DOCTYPE html>
<!--
CAB230 Web Computing Assignment 1
Authors: Heath Mayocchi n9378201
and Jacob Abell n9481869
-->
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Sign up - WiFi Hotspot Review</title>
		<script src="js/ass1script.js"></script>
		<!-- using defer src so the page can load up without waiting for the map script -->
		<script defer src="http://maps.googleapis.com/maps/api/js"></script>
		<link href="css/ass1style.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<!-- Set a wrapper for page width and alignment -->
		<div id="wrapper">
			<?php include 'php/header.inc'; ?>
			<?php include 'php/sidebar.inc'; ?>
			<div id="section">
				<div id="registration">
					<h2>Enter you details<br>to register</h2>
					<form action="php/register.php" method="post">
						<div class="signupRow">
							<div class="signupLeft">
								<!-- ('signupText')[0] -->
								<h4>First name:</h4>
								<input id="fname" type="text" name="regFname" onchange="HideError('fnameMissing')" class="signupText">
								<br>
								<span id="fnameMissing" class="validateError">A valid First name is required</span>
							</div>
							<div class="signupRight">
								<!-- ('signupText')[1] -->
								<h4>Last name:</h4>
								<input id="lname" type="text" name="regLname" onchange="HideError('lnameMissing')" class="signupText">
								<br>
								<span id="lnameMissing" class="validateError">A valid Last name is required</span>
							</div>
						</div>
						<div class="signupRow">
							<div class="signupLeft">
								<!-- ('signupText')[2] -->
								<h4>Date of birth:</h4>
								<input id="regDOB" type="text" name="regThisDOB" onchange="HideError('dobMissing')" class="signupText">
								<br>
								<span id="dobMissing" class="validateError">1/01/01 to 31-12-2099</span>
							</div>
							<div class="signupRight">
								<!-- ('signupText')[3] -->
								<h4>Post code:</h4>
								<input id="regPostcode" type="text" name="regPO" onchange="HideError('postcodeMissing')" class="signupText">
								<br>
								<span id="postcodeMissing" class="validateError">Must be 4 digits</span>
							</div>
						</div>
						<div class="signupRow">
							<div class="signupLeft">
								<!-- ('signupText')[4] -->
								<h4>Email address:</h4>
								<input id="regEmail" type="text" name="regEm" onchange="HideError('emailMissing')" class="signupText">
								<br>
								<span id="emailMissing" class="validateError">me@email.com</span>
							</div>
							<div class="signupRight">
								<!-- ('signupText')[5] -->
								<h4>Confirm email address:</h4>
								<input id="confirmEmail" type="text" onchange="HideError('confirmEmailMissing')" class="signupText">
								<br>
								<span id="confirmEmailMissing" class="validateError">Emails do not match</span>
							</div>
					</div>
					<div class="signupRow">
							<div class="signupLeft">
								<!-- ('signupText')[6] -->
								<h4>Password <small>(min 8 characters)</small>:</h4>
								<input id="regPassword" type="password" name="regPass" onchange="HideError('passwordMissing')" class="signupText">
								<br>
								<span id="passwordMissing" class="validateError">At least 8 characters is required</span>
							</div>
							<div class="signupRight">
								<h4>Confirm password:</h4>
								<!-- ('signupText')[7] -->
								<input id="confirmPassword" type="password" onchange="HideError('confirmPass')" class="signupText">
								<br>
								<span id="confirmPass" class="validateError">Passwords do not match</span>
							</div>
					</div>
						<div id="SignupStyle">
							<button type="submit" id="signUp" class="button rBorder" onclick="return ValidateReg(this)">Sign Up</button>
						</div>
					</form>
				</div><!-- end registration -->
			</div><!-- end section -->
			<?php include 'php/footer.inc'; ?>
		</div><!-- end wrapper -->
		<div id="overlay"></div>
	</body>
</html>
