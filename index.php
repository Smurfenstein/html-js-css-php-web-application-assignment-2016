<!DOCTYPE html>
<!--
CAB230 Web Computing Assignment 1
Author: Heath Mayocchi n9378201
-->
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Home - WiFi Hotspot Review</title>
		<script src="js/ass1script.js"></script>
		<!-- using defer src so the page can load up without waiting for the map script -->
		<script defer src="http://maps.googleapis.com/maps/api/js"></script>
		<link href="css/ass1style.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<!-- Set a wrapper for page width and alignment -->
		<div id="wrapper">
			<?php include 'php/header.inc'; ?>
			<?php include 'php/sidebar.inc'; ?>
			<div id="section">
			<!-- hard coded reviews, this will come from the DB when it is setup -->
				<div id="reviewSection">
					<h3>Latest Reviews:</h3>
						<a href="review.php"><h4>Booker Place Park</h4></a>
						<p>Birkin Rd & Sugarwood St<br>Bellbowrie, 4070</p>
						<h4>Malcolm Reynolds</h4>
						<p>05/12/2015</p>
						<?php include 'php/starsOne.inc'; ?>
						<p>It's there, but it's not that good. I did manage to connect up but it was a very week signal, like what you would expect from trying to use an iphone to send deep space signals.</p>
						<p>We will have to find a better location, for some reason Jayne is upset with no decent WiFi signal, I think he is spending too much time in his bunk.</p>
						<br>
						<a href="review.php"><h4>Annerley Library Wifi</h4></a>
						<p>450 Ipswich Road<br>Annerley, 4103</p>
						<h4>Jayne Cobb</h4>
						<p>07/12/2015</p>
						<?php include 'php/starsThree.inc'; ?>
						<p>This internet is good, I can download most stuff I want at a decent speed. I download lots of HD videos, I don't think they like it, but what are they gunna do, haha.</p>
						<p>Sux having to be at the library to access it, too many people reading books for my likin. Still I'm glad Mal was ok with moving to this spot.</p>
						<br>
						<a href="review.php"><h4>Annerley Library Wifi</h4></a>
						<p>450 Ipswich Road<br>Annerley, 4103</p>
						<h4>Kaylee Frye</h4>
						<p>07/12/2015</p>
						<?php include 'php/starsFour.inc'; ?>
						<p>Peace at last, I can't beleive how bitchy Jayne gets with no WiFi.</p>
						<p>Nice spot to get some quiet and catch up on emails. Keep an eye out for Jayne though, he hogs the bandwidth...</p>
					</div>
			</div><!-- end section -->
			<?php include 'php/footer.inc'; ?>
		</div><!-- end wrapper -->
		<div id="overlay"></div>
	</body>
</html>
