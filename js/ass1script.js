/*
CAB230 Web Computing Assignment 1
Author: Heath Mayocchi n9378201
*/

/* Toggle login and map popup boxes */
function LoginBox() {
	document.getElementById("overlay").style.display = "block";
	document.getElementById("loginPopup").style.display = "block";
}
function LoginClose() {
	document.getElementById("overlay").style.display = "none";
	document.getElementById("loginPopup").style.display = "none";
}
function MapCloser() {
	document.getElementById("overlay").style.display = "none";
	document.getElementById("mapBox").style.display = "none";
}

/* Show dropdown list for suburb quick search */
function SuburbDropFunction() {
    document.getElementById('suburbDropdown').classList.toggle('show');
}

/* Show dropdown list for rating quick search */
function RatingDropFunction() {
    document.getElementById("ratingDropdown").classList.toggle("show");
}

/* Global onclick */
window.onclick = function(event) {
	/* Toggle suburb dropdown box */
	if (!event.target.matches('.suburbdropbtn')) {
		var suburbdropdowns = document.getElementsByClassName("suburbContent");
		var s;
		for (s = 0; s < suburbdropdowns.length; s++) {
			var suburbopenDropdown = suburbdropdowns[s];
			if (suburbopenDropdown.classList.contains('show')) {
				suburbopenDropdown.classList.remove('show');
			}
		}
	}
	/* Toggle rating dropdown box */
	if (!event.target.matches('.ratingdropbtn')) {
		var ratingdropdowns = document.getElementsByClassName("ratingContent");
		var r;
		for (r = 0; r < ratingdropdowns.length; r++) {
			var ratingopenDropdown = ratingdropdowns[r];
			if (ratingopenDropdown.classList.contains('show')) {
				ratingopenDropdown.classList.remove('show');
			}
		}
	}
}

/* Geolocation error messages */
function showMapError(error) {
	var msg = "";
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			msg = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			msg = "An unknown error occurred."
			break;
	}
	document.getElementById("locationSupport").innerHTML = msg;
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
	'Error: The Geolocation service failed.' :
	'Error: Your browser doesn\'t support geolocation.');
}

/**
 * This is to create and style the close button on the popup map.
 * The CenterControl adds a control to the map that closes it.
 * This constructor takes the control DIV as an argument.
 */
function CenterControl(controlDiv, map) {
  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = '#fff';
  controlUI.style.border = '2px solid #fff';
  controlUI.style.borderRadius = '3px';
  controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
  controlUI.style.cursor = 'pointer';
  controlUI.style.marginBottom = '22px';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to close the map';
  controlDiv.appendChild(controlUI);
  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.color = 'rgb(25,25,25)';
  controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
  controlText.style.fontSize = '16px';
  controlText.style.lineHeight = '38px';
  controlText.style.paddingLeft = '5px';
  controlText.style.paddingRight = '5px';
  controlText.innerHTML = 'Close';
  controlUI.appendChild(controlText);
  // Setup the click event listeners: run the map close function.
  controlUI.addEventListener('click', function() {
    MapCloser();
  });
}

/* Toggle error messages */
function HideError(eleID) {
	document.getElementById(eleID).style.visibility = "hidden";
}

/* VALIDATES USER REGISTRATION elements */
function ValidateReg(form) {
	var returnFalse = 0;
	if (!MatchPasswords()) {
		returnFalse = 1;
	}
	if (!CheckPassword()) {
		returnFalse = 1;
	}
	if (!MatchEmails()) {
		returnFalse = 1;
	}
	if (!CheckEmail()) {
		returnFalse = 1;
	}
	if (!CheckPostcode()) {
		returnFalse = 1;
	}
	if (!CheckDOB()) {
		returnFalse = 1;
	}
	if (!CheckLname()) {
		returnFalse = 1;
	}
	if (!CheckFname()) {
		returnFalse = 1;
	}
	if (returnFalse == 1) {
		return false;
	}
	else{
		return true;
	}
}
/* Checks the first name box only contains letters and has at least one letter entered */
function CheckFname() {
	var fname = document.getElementsByClassName("signupText")[0].value;
	var fnameregex = /^[A-Za-z]+$/;
	if (!fnameregex.test(fname)) {
		// Set error text to visible and focus on the text box
		document.getElementById("fnameMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/* Checks the last name box only contains letters and has at least one letter entered */
function CheckLname() {
	var lname = document.getElementsByClassName("signupText")[1].value;
	var lnameregex = /^[A-Za-z]+$/;
	if (!lnameregex.test(lname)) {
		// Set error text to visible and focus on the text box
		document.getElementById("lnameMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/*
Checks the DOB has format:
	- day: number from 0 to 31
	- either '/' or '-' or ' ' or '.'
	- month: number from 1 to 12
	- either '/' or '-' or ' ' or '.'
	- year: number from 1900 to 2099 or 00 to 99
*/
function CheckDOB() {
	var dob = document.getElementsByClassName('signupText')[2].value
	var dobRegex = /^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])[- /.]([0-9]|1[0-2]|0[1-9])[- /.]((19|20)\d\d|[0-9]{2,2})$/i;
	if (!dobRegex.test(dob)) {
		// Set error text to visible and focus on the dob box
		document.getElementById("dobMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/* Checks the post code is exactly 4 digits */
function CheckPostcode() {
	var postcode = document.getElementsByClassName('signupText')[3].value
	var pcRegex = /^[0-9]{4,4}$/i;
	if (!pcRegex.test(postcode)) {
		// Set error text to visible and focus on the postcode box
		document.getElementById("postcodeMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/*
Checks the email box contains :
	- at least one symbol that isn't '@'
	- an '@' symbol
	- at least one symbol that isn't '@'
	- a '.'
	- at least 2 characters from the a-z range
*/
function CheckEmail() {
	var email = document.getElementsByClassName('signupText')[4].value
	var emailregex = /^[^@]+@[^@]+\.[A-Za-z]{2,}$/;
	if (!emailregex.test(email)) {
		// Set error text to visible and focus on the text box
		document.getElementById("emailMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/* Compare the value for both email fields */
function MatchEmails() {
	var email = document.getElementsByClassName('signupText')[4].value
	var confirmEmail = document.getElementsByClassName('signupText')[5].value
	if (email != confirmEmail){
		// Change email error text and focus on the confirm email box
		document.getElementById("confirmEmailMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/* Checks the  box contains at least 8 characters */
function CheckPassword() {
	var password = document.getElementsByClassName('signupText')[6].value
	var passwordregex = /^.{8,}$/i;
	if (!passwordregex.test(password)) {
		// Set error text to visible and focus on the text box
		document.getElementById("passwordMissing").style.visibility = "visible";
		return false;
	}
	return true;
}
/* Compare the value for both password fields */
function MatchPasswords() {
	var password = document.getElementsByClassName('signupText')[6].value
	var confirmPassword = document.getElementsByClassName('signupText')[7].value
	if (password != confirmPassword){
		// Change password error text and focus on the confirm password box
		document.getElementById("confirmPass").style.visibility = "visible";
		return false;
	}
	return true;
}

/* VALIDATES the WRITE A REVIEW elements */
function CheckReview(form) {
	var returnFalse = 0;
	if (!checkComment()) {
		returnFalse = 1;
	}
	oneChecked = false;
	if (!CheckRating()) {
		returnFalse = 1;
	}
	if (!CheckHotspot()) {
		returnFalse = 1;
	}
	if (returnFalse == 1) {
		return false;
	}
	return true;
}
/* if there is no comment made, display error message */
function checkComment() {
	var checkComm = document.getElementById('comments').value;
	if (checkComm == "") {
		document.getElementById("commentError").style.visibility = "visible";
		return false;
	}
	return true;
}
/* if none of the radio boxes are selected, display error message */
function CheckRating() {
	var checkRadio = document.getElementsByTagName('input');
	for (var i = 0; i < checkRadio.length; i++) {
		if (checkRadio[i].checked) {
			oneChecked = true;
		}
	}
	if (!oneChecked){
		document.getElementById("ratingError").style.visibility = "visible";
		return false;
	}
	return true;
}
/* if there is no hotspot selected display the error message */
function CheckHotspot() {
	if (document.getElementsByName('selectName')[0].value == 'hotspot') {
		document.getElementById("hotspotError").style.visibility = "visible";
		return false;
	}
	return true;
}

/* VALIDATES the USER LOGIN form */
function ValidateLogin(form) {
	var returnFalse = 0;
	if (!CheckLoginEmail()) {
		returnFalse = 1;
	}
	if (!CheckLoginPass()) {
		returnFalse = 1;
	}
	if (returnFalse == 1) {
		return false;
	}
	return true;
}
/*
Checks the email box contains :
	- at least one symbol that isn't '@'
	- an '@' symbol
	- at least one symbol that isn't '@'
	- a '.'
	- at least 2 characters from the a-z range
*/
function CheckLoginEmail() {
	var name = document.getElementsByClassName('loginText')[0].value
	var nameregex = /^[^@]+@[^@]+\.[a-z]{2,}$/i;
	if (!nameregex.test(name)) {
		// Set error text to visible and focus on the text box
		document.getElementById("loginEmailError").style.visibility = "visible";
		document.getElementById("loginEmail").focus();
		return false;
	}
	return true;
}
/* Checks the  box contains at least 8 characters */
function CheckLoginPass() {
	var password = document.getElementsByClassName('loginText')[1].value
	var passwordregex = /^.{8,}$/i;
	if (!passwordregex.test(password)) {
		// Set error text to visible and focus on the text box
		document.getElementById("loginPassError").style.visibility = "visible";
		document.getElementById("loginPassword").focus();
		return false;
	}
	return true;
}

/* VALIDATES the SEARCH TEXT */
function ValidateSearch() {
	if (!CheckSearch()) {
		return false;
	}
	return true;
}
/* Checks the search text does not contain any numbers */
function CheckSearch() {
	var search = document.getElementsByClassName("searchTxt")[0].value;
	// Check for no entry
	if (search == "Search by name or location") {
		// Set error text to visible and focus on the text box
		document.getElementById("searchError").style.visibility = "visible";
		document.getElementById("searchText").focus();
		return false;
	}
	// Check for numbers
	var searchregex = /^[^0-9]+$/;
	if (!searchregex.test(search)) {
		// Set error text to visible and focus on the text box
		document.getElementById("searchError").style.visibility = "visible";
		document.getElementById("searchText").focus();
		return false;
	}
	return true;
}
