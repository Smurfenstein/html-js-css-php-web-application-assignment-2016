<!DOCTYPE html>
<!--
CAB230 Web Computing Assignment 1
Authors: Heath Mayocchi n9378201
and Jacob Abell n9481869
-->
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Location review - WiFi Hotspot Review</title>
		<script src="js/ass1script.js"></script>
		<!-- using defer src so the page can load up without waiting for the map script -->
		<script defer src="http://maps.googleapis.com/maps/api/js"></script>
		<link href="css/ass1style.css" rel="stylesheet" type="text/css" />
		<?php $reviewHotspot = $_GET["h"]; ?>
	</head>
	<body onload="showRevMap()">
		<?php include 'php/revMap.inc'; ?>
		<!-- Set a wrapper for page width and alignment -->
		<div id="wrapper">
			<?php include 'php/header.inc'; ?>
			<?php include 'php/sidebar.inc'; ?>
			<div id="section">
				<div id="sectionHead">
					<div id="reviewHeader">
						<div id="reviewMap" class="rBorder reMap"></div>
						<div id="reviewHotspot">
							<?php include 'php/reviewContent.inc'; ?>							
				</div>
			</div><!-- end section -->
			<?php include 'php/footer.inc'; ?>
		</div><!-- end wrapper -->
		<div id="overlay"></div>
	</body>
</html>
