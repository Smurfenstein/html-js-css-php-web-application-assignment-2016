<!DOCTYPE html>
<!--
CAB230 Web Computing Assignment 1
Author: Heath Mayocchi n9378201
-->
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Write a review - WiFi Hotspot Review</title>
		<script src="js/ass1script.js"></script>
		<!-- using defer src so the page can load up without waiting for the map script -->
		<script defer src="http://maps.googleapis.com/maps/api/js"></script>
		<link href="css/ass1style.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<!-- Set a wrapper for page width and alignment -->
		<div id="wrapper">
			<?php include 'php/header.inc'; ?>
			<?php include 'php/sidebar.inc'; ?>
			<div id="section">
				<form id="makeReview" action="php/review.php" method="get">
					<div id="writeReview">
						<h2>Write your review</h2>
						<h4>Select a WiFi hotspot to review</h4>
						<br>
						<div class="selectHS">
							<select id="selectHotspot" class="selectClass rBorder" name="selectName" onchange="HideError('hotspotError')">
								<option value="hotspot">Hotspot List</option>
								<option value="annerleyLibrary">Annerley Library Wifi</option>
								<option value="orielPark">Oriel Park</option>
								<option value="ashgroveLibrary">Ashgrove Library Wifi</option>
								<option value="banyoLibrary">Banyo Library Wifi</option>
							</select>
						</div>
						<div id="hotspotError" class="validateError">
							<p>Please select a hotspot</p>
						</div>
						<div id="selectRating">
							<h4>Select a rating</h4>
							<?php include 'php/starsFive.inc'; ?>
							<br>
								<input type="radio" name="starRating" value="one" onchange="HideError('ratingError')"> One
								<input type="radio" name="starRating" value="two" onchange="HideError('ratingError')"> Two
								<input type="radio" name="starRating" value="three" onchange="HideError('ratingError')"> Three
								<input type="radio" name="starRating" value="four" onchange="HideError('ratingError')"> Four
								<input type="radio" name="starRating" value="five" onchange="HideError('ratingError')"> Five
							<div id="ratingError" class="validateError">
								<p>Please select a rating</p>
							</div>
						</div>
						<div id="writeComments">
							<h4>Let us know about your experience at this hotspot</h4><br>
							<textarea id="comments" cols="50" rows="10" placeholder="" name="thisComment" onkeyup="HideError('commentError')"></textarea>
							<div id="commentError" class="validateError">
								<p>Please include a useful comment</p>
							</div>
						</div>
						<div id="submitReview">
							<button type="submit" class="button rBorder" onclick="return CheckReview(this)">Submit</button>
						</div>
					</div>
				</form>
			</div><!-- end section -->
			<?php include 'php/footer.inc'; ?>
		</div><!-- end wrapper -->
		<div id="overlay"></div>
	</body>
</html>
